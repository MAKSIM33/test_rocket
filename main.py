import sys

import numpy as np
import scipy.signal as signal


def read_targets(file_path):
    targets = []
    with open(file_path, 'r') as file:
        for line in file:
            x, y, w = map(int, line.split())
            targets.append((x, y, w))
    return targets


def get_optimal_strike(targets, r):
    grid_size = 100
    grid = np.zeros((grid_size, grid_size), dtype=int)

    for x, y, v in targets:
        grid[x, y] += v

    kernel = np.zeros((2 * r + 1, 2 * r + 1), dtype=int)
    for i in range(2 * r + 1):
        for j in range(2 * r + 1):
            if (i - r) ** 2 + (j - r) ** 2 <= r ** 2:
                kernel[i, j] = 1

    conv_result = signal.fftconvolve(grid, kernel, mode='same')
    max_value = np.max(conv_result)
    max_indices = np.argwhere(conv_result == max_value)

    x, y = max_indices[0]
    total_value = np.sum(grid[x - r:x + r + 1, y - r:y + r + 1])

    return x, y, total_value


if __name__ == '__main__':
    path = sys.argv[1]
    R = int(sys.argv[2])
    result = get_optimal_strike(read_targets(file_path=path), r=R)
    print(f"{result[0]} {result[1]} {result[2]}")

